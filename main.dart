import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Ruta inicial: Productos
      initialRoute: '/',
      routes: {
        // Rutas
        //'/': (context) => Principal(),
        '/products': (context) => Products(),
        '/providers': (context) => Providers(),
      },

      debugShowCheckedModeBanner: false,
      title: 'Consumiendo api',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Consumo de API'),
        ),
        body: Center(
          child: ElevatedButton(
            child: Text('Productos'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return Products();
              }));
            },
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black87,
                ),
                child: Text(
                  'Menú',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.article),
                title: Text('Productos'),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) {
                      return Products();
                    }),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.group),
                title: Text('Proveedores'),
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) {
                      return Providers();
                    }),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('Settings'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Clase para módulo de Productos
class Principal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Principal')),
      body: Center(
        child: ElevatedButton(
          child: Text('Home'),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return MyApp();
            }));
          },
        ),
      ),
    );
  }
}

// Clase para módulo de Productos
class Products extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Productos')),
      body: Center(
        child: Text('Pantalla para mostrar productos'),
      ),
    );
  }
}

// Clase para módulo de Proveedores
class Providers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Proveedores')),
      body: Center(
        child: Text('Pantalla para mostrar proveedores'),
      ),
    );
  }
}

// Clase para módulo mascotas


